<?php 
// koneksi ke DBMS
// $conn = mysqli_connect("localhost", "root", "", "pendaftarseminar");

require 'functions.php';

// cek apakah tombol submit sudah ditekan atau belum
if ( isset($_POST["submit"]) ) {
	

	
	// cek apakah data berhasil ditambahkan atau tidak
	if ( tambah($_POST) > 0) {
		echo "
			<script>
				alert('Data ditambahkan!');
				document.location.href = 'index.php';
			</script>
		";
	} else {
		echo "
			<script>
				alert('Error, data gagal ditambah!');
				document.location.href = 'index.php';
			</script>
		";
	}
}
 ?>


<!DOCTYPE html>
<html>
<head>
	<title>Data Peserta</title>
	<link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
	
	<h1>Tambah Peserta</h1>

	<form action="" method="post">
		<ul>
			<li>
				<label for="nama">Nama : </label>
				<input type="text" name="nama" id="nama" required>
			</li>
			<li>
				<label for="ttl">TTL :</label>
				<input type="text" name="ttl" id="ttl" required>
			</li>
			<li>
				<label for="email">Email :</label>
				<input type="text" name="email" id="email" >
			</li>
			<li>
				<label for="no_hp">No. Hp :</label>
				<input type="text" name="no_hp" id="no_hp" >
			</li>
			<li>
				<label for="alamat">Alamat :</label>
				<input type="text" name="alamat" id="alamat" >
			</li>
			<li>
				<button type="submit" name="submit">Tambah data</button>
			</li>
		</ul>

	</form>
	


</body>
</html>