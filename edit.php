<?php 
// koneksi ke DBMS
// $conn = mysqli_connect("localhost", "root", "", "pendaftarseminar");

require 'functions.php';

// ambil data di URL
$id = $_GET["id"];
// queri data mahasiswa berdasarkan id
$str = query("SELECT * FROM peserta WHERE id = $id")[0];


// cek apakah tombol submit sudah ditekan atau belum
if ( isset($_POST["submit"]) ) {
	

	
	// cek apakah data berhasil diubah atau tidak
	if ( edit($_POST) > 0) {
		echo "
			<script>
				alert('Data diedit!');
				document.location.href = 'index.php';
			</script>
		";
	} else {
		echo "
			<script>
				alert('Error, data gagal diedit!');
				document.location.href = 'index.php';
			</script>
		";
	}
}
 ?>


<!DOCTYPE html>
<html>
<head>
	<title> Edit Data Peserta</title>
</head>
<body>
	<h1>Edit Tambah Peserta</h1>

	<form action="" method="post">
		<input type="hidden" name="id" value="<?= $str["id"]; ?>">
		<ul>
			<li>
				<label for="nama">Nama : </label>
				<input type="text" name="nama" id="nama" required value="<?= $str["nama"]; ?>">
			</li>
			<li>
				<label for="ttl">TTL :</label>
				<input type="text" name="ttl" id="ttl" required value="<?= $str["ttl"]; ?>">
			</li>
			<li>
				<label for="email">Email :</label>
				<input type="text" name="email" id="email" value="<?= $str["email"]; ?>">
			</li>
			<li>
				<label for="no_hp">No. Hp :</label>
				<input type="text" name="no_hp" id="no_hp" value="<?= $str["no_hp"]; ?>">
			</li>
			<li>
				<label for="alamat">Alamat :</label>
				<input type="text" name="alamat" id="alamat" value="<?= $str["alamat"]; ?>">
			</li>
			<li>
				<button type="submit" name="submit">Edit data</button>
			</li>
		</ul>

	</form>
</body>
</html>