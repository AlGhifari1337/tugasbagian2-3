<?php 
// menghubungkan halaman index ke halaman function
require 'functions.php';
$str = query("SELECT * FROM peserta");

// tombol search ditekan
if ( isset($_POST["search"]) ) {
	$str = search($_POST["keyword"]);
}


?>
<!DOCTYPE html>
<html>
<head>
	<title>Admin Page</title>
	<style type="text/css">
		* {
			background-image: url(background.png);		}

	</style>
</head>
<body>

<h1 align="center">Data Pendaftar</h1>

<a href="tambah.php">Tambah Peserta</a>
<br><br>

<form action="" method="post">
	
	<input type="text" name="keyword" size="40" autofocus placeholder="Search" autocomplete="off">
	<button type="submit" name="search">Search</button>

</form>
<br>

<table border="1" cellpadding="10" cellspacing="0">

	<tr>
		<th>No.</th>
		<th>Aksi</th>
		<th>Nama</th>
		<th>TTL</th>
		<th>Email</th>
		<th>No. Hp</th>
		<th>Alamat</th>
	</tr> 

	<?php $i = 1; ?>
	<?php foreach($str as $row ) : ?>
	<tr>
		<td><?= $i; ?></td>
		<td>
			<a href="edit.php?id=<?= $row["id"]; ?>">Edit</a> |
			<a href="delete.php?id=<?= $row["id"]; ?>" onclick="return confirm('Ingin Menghapus?');">Delete</a>
		</td>

		<td><?= $row["nama"]; ?></td>
		<td><?= $row["ttl"]; ?></td>
		<td><?= $row["email"]; ?></td>
		<td><?= $row["no_hp"]; ?></td>
		<td><?= $row["alamat"]; ?></td>
		
	</tr>
<?php $i++; ?>
<?php endforeach; ?>


</table>
</body>
</html>




